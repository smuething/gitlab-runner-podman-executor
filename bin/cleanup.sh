#!/usr/bin/env bash

currentDir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
source ${currentDir}/base.sh

if podman pod exists "${POD_ID}" >/dev/null 2>&1; then
    echo 'Removing pod ${POD_ID}'
    log podman pod kill "${POD_ID}"
    log podman pod rm -f "${POD_ID}"
fi

touch "${JOB_DIR}/done"

exit 0
