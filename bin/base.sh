CONTAINER_ID="runner-$CUSTOM_ENV_CI_RUNNER_ID-project-$CUSTOM_ENV_CI_PROJECT_ID-job-$CUSTOM_ENV_CI_JOB_ID"
POD_ID="pod-$CUSTOM_ENV_CI_RUNNER_ID-project-$CUSTOM_ENV_CI_PROJECT_ID-job-$CUSTOM_ENV_CI_JOB_ID"
HOSTNAME=gitlab-ci

if [[ -z ${CUSTOM_ENV_CI_JOB_IMAGE} ]] ; then
    echo "Missing container image specification, cannot start job!"
    exit $SYSTEM_FAILURE_EXIT_CODE
fi

IMAGE="${CUSTOM_ENV_CI_JOB_IMAGE}"

BUILD_ROOT="$HOME/_jobs"

JOB_DIR="${BUILD_ROOT}/runner-$CUSTOM_ENV_CI_RUNNER_ID-project-$CUSTOM_ENV_CI_PROJECT_ID-job-$CUSTOM_ENV_CI_JOB_ID"
LOG_FILE="${JOB_DIR}/log.txt"
BUILD_DIR="${JOB_DIR}/build"
BUILD_MOUNT_DIR="$(dirname "${CUSTOM_ENV_CI_PROJECT_DIR}")"
CONTAINER_DIR="${JOB_DIR}/containers"
CACHE_DIR="${JOB_DIR}/cache"
GITLAB_RUNNER_VERSION="$(gitlab-runner -v | grep Version | awk '{ print $2 ; }' | sed 's/~/-/' )"
GITLAB_HELPER_IMAGE="registry.gitlab.com/smuething/gitlab-runner-podman-executor/gitlab-runner-helper:${GITLAB_RUNNER_VERSION}"

log() {
    "$@" >> "${LOG_FILE}" 2>&1
}
