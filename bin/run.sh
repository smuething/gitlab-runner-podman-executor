#!/usr/bin/env bash

currentDir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
source ${currentDir}/base.sh

exit_code=0

case $2 in

    'build_script' | 'after_script' )
        shell=/bin/bash
        log podman run --detach --interactive --tty \
               --pod "${POD_ID}" \
               --name "${CONTAINER_ID}" \
               --volume "${CACHE_DIR}":/builds/cache:Z \
               --volume "${BUILD_DIR}":"${BUILD_MOUNT_DIR}":Z \
               --volume "${CONTAINER_DIR}":"/var/lib/containers":Z \
               --tmpfs  "/var/run/containers" \
               --volume "$1":/gitlab-ci-script.sh:Z \
               --device /dev/fuse:rw \
               --env "DUNECI_PARALLEL=4" \
               --env-file "${currentDir}/container.env" \
               --cpus 4.0 \
               --memory 16g \
               ${IMAGE}

        # fix access rights in the build directory if not running as root in the container
        user="$(podman inspect "${IMAGE}" --format "{{.User}}")"
        if [[ -n $user ]] ; then
            log podman exec --user 0 "${CONTAINER_ID}" chown -R "$user" "${CUSTOM_ENV_CI_PROJECT_DIR}/.."
        fi

        # make sure the script can also be run if the user inside the container is not root
        chmod 755 "$1"
        ;;
    *)
        shell=/bin/sh
        log podman run --detach --interactive --tty \
               --pod "${POD_ID}" \
               --name "${CONTAINER_ID}" \
               --volume "${CACHE_DIR}":/builds/cache:Z \
               --volume "${BUILD_DIR}":"${BUILD_MOUNT_DIR}":Z \
               --volume "$1":/gitlab-ci-script.sh:Z \
               --env-file "${currentDir}/container.env" \
               --cpus 4.0 \
               --memory 16g \
               ${GITLAB_HELPER_IMAGE} >/dev/null
        ;;
esac

podman exec "$CONTAINER_ID" "$shell" /gitlab-ci-script.sh
if [ $? -ne 0 ]; then
    # Exit using the variable, to make the build as failure in GitLab
    # CI.
    exit_code=$BUILD_FAILURE_EXIT_CODE
fi

log podman kill "${CONTAINER_ID}"
log podman rm -f "${CONTAINER_ID}"

exit ${exit_code}
