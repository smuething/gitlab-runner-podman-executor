#!/usr/bin/env bash

set -Eeuo pipefail

cat <<EOF
{
  "builds_dir" : "/builds",
  "cache_dir" : "/builds/cache",
  "builds_dir_is_shared" : false,
  "hostname" : "$(hostname)",
  "driver" : {
    "name" : "gitlab-runner-podman-executor",
    "version" : "v0.1.0"
  }
}
EOF
