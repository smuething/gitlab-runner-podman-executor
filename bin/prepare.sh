#!/usr/bin/env bash

currentDir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
source ${currentDir}/base.sh

set -Eeuo pipefail

handle_error() {
    echo >&2 "Error setting up job environment, aborting"
    log podman pod rm -f ${POD_ID}
    exit ${SYSTEM_FAILURE_EXIT_CODE}
}

# trap any error, and mark it as a system failure.
trap handle_error ERR

prepare_pod() {

    mkdir -p "${JOB_DIR}"

    if podman inspect "$CONTAINER_ID" >/dev/null 2>&1; then
        echo 'Found old container, deleting'
        log podman kill "$CONTAINER_ID"
        log podman rm "$CONTAINER_ID"
    fi

    if podman pod exists "$POD_ID" >/dev/null 2>&1; then
        echo 'Found old pod, deleting'
        log podman pod kill "$POD_ID"
        log podman pod rm -f "$POD_ID"
    fi

    mkdir -p "$CACHE_DIR"
    mkdir -p "$CONTAINER_DIR"
    mkdir -p "$BUILD_DIR"

    podman login -u $CUSTOM_ENV_CI_REGISTRY_USER --password-stdin <<<"$CUSTOM_ENV_CI_REGISTRY_PASSWORD" $CUSTOM_ENV_CI_REGISTRY
    podman pull "$GITLAB_HELPER_IMAGE"
    podman pull "$IMAGE"

    log podman pod create --name "${POD_ID}" --hostname "${HOSTNAME}"

    echo Setting up network
    log podman run --detach --interactive --tty \
           --pod "${POD_ID}" \
           --name "${CONTAINER_ID}" \
           --cap-add CAP_NET_ADMIN \
           --volume "${currentDir}/setup-network.sh":/setup-network.sh:Z \
           ${GITLAB_HELPER_IMAGE}

    podman exec "${CONTAINER_ID}" /bin/sh /setup-network.sh
    echo done

    log podman kill "${CONTAINER_ID}"
    log podman rm -f "${CONTAINER_ID}"
}

echo "Running in pod $POD_ID"

prepare_pod
