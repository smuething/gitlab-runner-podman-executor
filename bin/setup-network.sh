#!/bin/bash

set -euo pipefail

# load basic table and chain setup
tables="
table ip filter {
  chain input {
    type filter hook input priority 0; policy accept;
  }

  chain forward {
    type filter hook forward priority 0; policy drop;
  }

  chain output {
    type filter hook output priority 0; policy drop;
    ip daddr 127.0.0.0/8 accept
    ip daddr 10.0.2.3 udp dport 53 accept
    ip daddr 10.10.0.1 tcp dport 3128 accept
  }
}"

nft "${tables}"
