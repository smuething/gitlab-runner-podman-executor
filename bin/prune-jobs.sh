#!/bin/bash

set -Eeuo pipefail

file_age () {
    echo $(($(date +%s) - $(date +%s -r "$1")))
}

BUILD_ROOT=/home/gitlab-runner/_jobs

for job in ${BUILD_ROOT}/* ; do

    if [[ ! -f ${job}/done ]] ; then
        continue
    fi

    age=$(file_age "${job}/done")

    if ((${age} < 300)) ; then
        continue
    fi

    jobname=$(basename "${job}")
    echo "Pruning job ${jobname}"
    rm -rf "${job}"

done
